import axios from "axios";

axios.defaults.baseURL = process.env.REACT_APP_HOST_URL;

export const operationbyUserId = async (id, date_start, date_end) => {
    try {
        let operation = await axios.get('/operations/user/' + id, {
            params: {
                start: date_start,
                end: date_end
            }
        });
        return operation.data
    } catch (error) {
        console.log(error);
        return null
    }
}

export const updateOp = async (item) => {
    try {
        if (item != null) {
            let data = await axios.patch('/operations/' + item.op_id, {
                amount: item.amount,
                category_id: item.category_id,
                description: item.description,
                user_id: item.user_id,
                date: item.date
            })
            console.log(data);
            return data.status
        }
    } catch (error) {
        console.log(error);
        return null
    }
}

export const addOperation = async (item) => {
    try {
        if (item != null) {
            let data = await axios.post('/operations/', {
                amount: item.amount,
                category_id: item.category_id,
                description: item.description,
                user_id: item.user_id,
                date: item.date
            })
            console.log(data);
            return data.status
        }
    } catch (error) {
        console.log(error);
        return null
    }
}

export const deleteOperations = async (id) =>{
    try {
        let status = await axios.delete('/operations/'+id);
        console.log(status);
    } catch (error) {
        console.log(error);
        return null
    }
}