import axios from "axios";

axios.defaults.baseURL = process.env.REACT_APP_HOST_URL;

export async function budgetAll() {
    try {
        let users = await axios.get('/budget');
        
        return users.data;
    } catch (error) {
        console.log(error);
        return null
    }


}

export async function budgetById(id) {

    try {
        let user = await axios.get('/budget/' + id);
        return user.data;
    } catch (error) {
        console.log(error);
        return null
    }

}

export async function budgetAdd(item){
    try {
        if (item != null) {
            let data = await axios.post('/budget', {
                name:item.name,
                total: item.total,
                date_start:item.date_start,
                date_end:item.date_end
            })
            console.log(data);
            return data.status
        }
        
    } catch (error) {
    if (error.message.includes('422')) {
        return 422
    } else if(error.message.includes('500')){
        return 500
    }
    }
}

export async function deleteBudget(id){
    try {
        await axios.delete('/budget/'+id)
    } catch (error) {
        console.log(error);
    }
}

export async function updateBudget(item){
    try {
        if (item != null) {
            let data = await axios.patch('/budget/'+item.user_id, {
                name: item.name,
                total: item.total,
                date_start: item.date_start,
                date_end: item.date_end
            });
            console.log(data.status);
            return data.status;
            
        }
    } catch (error) {
        console.log(error);
    }
}
