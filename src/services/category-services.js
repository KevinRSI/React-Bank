import axios from "axios";

axios.defaults.baseURL = process.env.REACT_APP_HOST_URL;


export const findAllCat = async (id) => {
    let data = await axios.get('/category')
    return data.data
}