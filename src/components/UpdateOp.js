import "moment/locale/fr";
import moment from "moment";
import { useEffect, useState } from "react"
import { updateOp } from "../services/operation-services"

export const UpdateOp = ({input, category, onSave, newValue}) => {
    const [operation, setOperation] = useState(null);
    const [change, setChange] = useState(null);

    
    useEffect(() => {
        async function sendData() {
            let status = await updateOp(operation)
            if (status === 200) {
                onSave()
                newValue({...input, ...operation});
            }
        }
        sendData()
    }, [operation, onSave, newValue, input])

    function handleSubmit(event) {
        event.preventDefault();
        if (!change) {
            onSave()
        }
        setOperation(change);
        
    }

    function handleChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        let data = { [name]: value };
        
        setChange({...{ ...change, ...data }, op_id:input.op_id})
    }

    return <div key={input.op_id} className="list-group-item  list-group-item-action flex-column align-items-start bg-success text-light mb-1">
        <form onSubmit={handleSubmit}>
            <div className="d-flex w-100 justify-content-between">
                <input required className='col-4 rounded form-control-sm' placeholder='€' type="number" name="amount" id="amount-add" step='0.01' onChange={handleChange} defaultValue={Number(input.amount).toFixed(2)} />
                <input required className='col-7 rounded form-control-sm' placeholder='date' type="date" name="date" id="date-add" onChange={handleChange}  defaultValue={moment(input.date).format('YYYY-MM-DD')}/>
            </div>
            <input required type="text" placeholder='description' name='description' className='col-12 rounded form-control-sm' onChange={handleChange} defaultValue={input.description} />
            <select required className='rounded form-control-sm col-8' name="category_id" onChange={handleChange} id="">
                {category.map((value,index)=>{return <option value={value.id} key={index}>{value.name}</option>})}
            </select>
            <button className=' m-1 btn btn-warning' type="submit">save</button>
        </form>
    </div>
}