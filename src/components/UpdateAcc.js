import moment from "moment";
import { useState } from "react"
import { updateBudget } from "../services/budget-service";

export const UpdateUser = ({ close, value, id }) => {
    const [change, setChange] = useState({});

    

    async function handleSubmit(event) {
        try {
            event.preventDefault();
            let data = await updateBudget({ ...change, user_id: id });
            if (data===200) {
                close()
            }
            
        } catch (error) {
            console.log(error);
        }
    }

    function handleChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        let data = { [name]: value };

        setChange({ ...change, ...data })
    }

    return (



        <div className=' offset-md-1 offset-lg-2 col-12 mt-5 bg-success my-auto col-md-4 card text-light'>
            <form action="" onSubmit={handleSubmit}>
                <div className="card-body">
                    <input required className='form-control-sm' placeholder='Nom' type="text" name="name" id="acc-addName" defaultValue={value.name} onChange={handleChange} />
                    <p className='card-text my-1'>Sur la période du <br></br>
                        <input required defaultValue={moment(value.date_start).format('YYYY-MM-DD')} className='form-control-sm col-12 col-lg-5 mt-1' type="date" name="date_start" id="date-addAcc" onChange={handleChange}/> 
                        au 
                        <input required defaultValue={moment(value.date_end).format('YYYY-MM-DD')} className='form-control-sm col-12 col-lg-5' type="date" name="date_end" id="date-addAcc" onChange={handleChange} /> </p>
                    <input required className='form-control-sm mt-1' placeholder='Montant total' type="number" name="total" defaultValue={value.total} step='0.01' id="total-addAcc" onChange={handleChange}/>
                    <div className='mt-2'><button className='btn btn-warning' type="submit">Modifier</button></div>
                </div>
            </form>
        </div>


    )
}