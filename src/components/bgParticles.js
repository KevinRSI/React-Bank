import Particles from 'react-tsparticles';

/**
 * 
 * @returns particles for the background
 */
export const BGPart =()=>{
    return (
        <Particles className="particles" options={{
            background: {
                
            },
            fpsLimit: 25,
            interactivity: {
                detectsOn: "canvas",
                events: {
                    onClick: {
                        enable: false,
                        mode: "repulse",
                    },
                    onHover: {
                        enable: false,
                        mode: "repulse",
                    },
                    resize: true,
                },
                modes: {
                    bubble: {
                        distance: 600,
                        duration: 0.4,
                        opacity: 1,
                        size: 40,
                    },
                    push: {
                        quantity: 4,
                    },
                    repulse: {
                        distance: 200,
                        duration: 0.4,
                    },
                },
            },
            particles: {
                color: {
                    value: "#5cb85c",
                },
                links: {
                    color: "#5cb85c",
                    distance: 150,
                    enable: true,
                    opacity: 1,
                    width: 1,
                },
                collisions: {
                    enable: true,
                },
                move: {
                    direction: "none",
                    enable: true,
                    outMode: "bounce",
                    random: false,
                    speed: 1,
                    straight: false,
                },
                number: {
                    density: {
                        enable: true,
                        value_area: 800,
                    },
                    value: 40,
                },
                opacity: {
                    value: 0.8,
                },
                shape: {
                    type: "square",
                },
                size: {
                    random: true,
                    value: 6,
                },
            },
            detectRetina: true,
        }} > </Particles>
    )
}