import "moment/locale/fr";
import { useState } from "react"
import { addOperation } from "../services/operation-services";


export const AddOp = ({ done, category, id, refresh }) => {
    const [change, setChange] = useState({});
    const [error, setError] = useState(false);
    async function sendData() {
        let status = await addOperation({ ...change, user_id: id })
        console.log({ ...change, user_id: id });
        if (status === 201) {
            setChange({});
            done()
        }
    }


    function handleSubmit(event) {
        event.preventDefault();

        if (change.hasOwnProperty('category_id') && change.hasOwnProperty('amount') && change.hasOwnProperty('date')) {
            sendData();
        } else {
            setError(true)
        }
    }

    function handleChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        let data = { [name]: value };

        setChange({ ...change, ...data })
    }

    return <div className="list-group-item overflow-hidden list-group-item-action flex-column align-items-start bg-success text-light mb-1">
        <form onSubmit={handleSubmit}>
            <div className="d-flex justify-content-between">
                <input required className='col-4 rounded form-control-sm' placeholder='€' type="number" name="amount" id="amount-add" step='0.01' onChange={handleChange} />
                <input required className='col-7 rounded form-control-sm' placeholder='date' type="date" name="date" id="date-add" onChange={handleChange} />
            </div>
            <input required type="text" placeholder='description' name='description' className='col-12 rounded form-control-sm' onChange={handleChange} />
            <select required defaultValue='null' className='rounded form-control-sm col-8' name="category_id" onChange={handleChange} id="">
                <option value="null" disabled hidden> Catégories </option>
                {category.map((value, index) => { return <option value={value.id} key={index}>{value.name}</option> })}
            </select>
            <button className=' m-1 btn btn-warning' type="submit">Créer</button>
            {error && <p className='text-light font-weight-bold rounded bg-danger'>Données manquantes</p>}
        </form>
    </div>
}