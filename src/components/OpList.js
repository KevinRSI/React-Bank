import "moment/locale/fr";
import moment from "moment";
import { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import { UpdateOp } from "./UpdateOp";
import { deleteOperations } from "../services/operation-services";


export const OpList = ({ value, category, refresh }) => {

    const [edit, setEdit] = useState(false);
    const [op, setOp] = useState({});
    const [loading, setLoading] = useState(true);
    const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

    function catName(catId) {
        return category.find(item => item.id === catId).name
    }

    function editChange(param){
        setEdit(param);
    }

    function deleteOp(id){
        deleteOperations(id);
        refresh(true);
    }

    function changeValue(param){
        setOp(param);
        setLoading(false);
    }
    
    
    
    useEffect(() => {
        changeValue(value);
        ReactTooltip.rebuild()
    }, [value])



    return !loading && !edit ? <div key={op.op_id}  className="list-group-item  list-group-item-action flex-column align-items-start bg-success text-light mb-1">
        <div className="d-flex w-100 justify-content-around">
            <button onClick={() => editChange(true)} className="btn position-absolute btn-success top-0 end-0 text-light far fa-edit"></button>
            <button onClick={() => deleteOp(value.op_id)} className="btn btn-success mb-2 col-2 text-warning "><i className="fas fa-trash"></i></button>


            <h5 className="position-absolute start-0 mx-1">{Number(op.amount).toFixed(2)}€</h5>


        </div>
        <div className=" col-12  d-flex justify-content-center">
            <h6 data-tip data-for={op.description} className=" bg-light text-truncate col-9 text-success">{op.description}</h6>
        </div>
        <div className="d-flex justify-content-between">
            <small>{catName(op.category_id)}</small>
            <small className="" data-tip data-for={op.date}>{moment(op.date).fromNow()}</small>

        </div>
        <ReactTooltip id={op.description} place='top' delayShow={100} delayHide={200} >
            <div>{op.description}</div>
        </ReactTooltip>

        <ReactTooltip id={op.date} place='right' effect='solid' delayShow={100} delayHide={200} disable={isMobile}>
            <div>{moment(op.date).format('ll')}</div>
        </ReactTooltip>
    </div> : <UpdateOp input={op} category={category} onSave={()=>editChange(false)} newValue={changeValue}/>



}