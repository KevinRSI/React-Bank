import { useEffect, useState } from "react"
import { budgetAdd } from "../services/budget-service";

export const AddUser = ({ onOpen }) => {
    const [user, setUser] = useState(null);
    const [change, setChange] = useState({});
    const [done, setDone] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {

        async function sendData() {
            let status = await budgetAdd(user)
            if ( status === 201) {
                setDone(true);
            } else if (status === 422) {
                setError(status)
            }
        }
        sendData()
    }, [user])

    function handleSubmit(event) {
        event.preventDefault();
        setUser(change);
        console.log(change);
    }

    function handleChange(event) {
        let name = event.target.name;
        let value = event.target.value;
        let data = { [name]: value };

        setChange({ ...change, ...data })
    }

    return (


        <div className='row w-100 flex-direction-column bg-light vh-100 vw-100'>
            <div className="text-start position-relative mt-2">
                <button className='btn btn-outline-success' onClick={() => onOpen(false)}>Retour</button>
            </div>

            <div className="text-align-center text-light shadow justify-content-center card add bg-success col-md-4 col-10 mx-auto rounded">

                {done === false ? <form className='' action="" onSubmit={handleSubmit}>
                    <h3>Ajouter un utilisateur</h3>
                    <div className="form-group">
                        <label htmlFor="Name" className='form-label'>Nom de l'utilisateur</label>
                        <input type="text" name="name" className='form-control form-control-sm' onChange={handleChange} /><br />
                    </div>
                    <div className="form-group">
                        <label htmlFor="total" className='col-forms'>Total du mois</label>
                        <input type="number" step='0.01' name="total" className='form-control form-control-sm' id="total_month" onChange={handleChange} /><br />
                    </div>
                    <div className="row">
                        <div className="col">
                            <label htmlFor="date_start">Date de début</label>
                            <input type="date" name="date_start" className='form-control form-control-sm' id="date_start" onChange={handleChange} /><br />
                        </div>
                        <div className="col">
                            <label htmlFor="date_end" className='col-forms'>Date de fin</label>
                            <input type="date" name="date_end" className='form-control form-control-sm' id="date_end" onChange={handleChange} /><br />
                        </div>
                    </div>

                    <br />
                    <button type="submit" className='btn rounded btn-outline-light'>Créer</button>

                    {error === 422 ? <p className='text-danger'>Veuillez vérifier vos informations</p> : error === 500 ? <p className='text-danger'>Une erreur est survenue veuillez réessayer dans un moment</p> : null}
                </form> : window.location.reload()}
            </div>
        </div>
    )
}