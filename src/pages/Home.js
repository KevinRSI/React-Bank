import { useEffect, useState } from "react"
import 'bootstrap/dist/css/bootstrap.css';
import ReactTooltip from 'react-tooltip';

import { Link } from "react-router-dom";
import { budgetAll, deleteBudget } from "../services/budget-service";
import { AddUser } from "../components/AddAcc";


export const Home = () => {
    const [userData, setUserData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [open, setOpen] = useState(false);

    

    function switchOpen(param) {
        setOpen(param);
    }

    function deleteEntry(id) {
        deleteBudget(id);
        setUserData(
            userData.filter(item => item.id !== id)
        )
    }
    
    useEffect(() => {
        async function fetchData() {
            setUserData(await budgetAll(), setLoading(false));
            ReactTooltip.rebuild()
        }
        fetchData()
    }, [])


    return (
        open === false ?
            <div className="row  flex-direction-column bg-light vh-100 vw-100">
                <div className="text-align-center text-light shadow justify-content-center card home bg-success col-md-11 col-10 m-auto rounded">
                    <div className="title mb-md-5 mb-2">
                        <h1 className="" >Budget App</h1>
                        <h4 className=''>Choisir un utilisateur pour continuer : </h4>
                    </div>
                        <div>
                            {(loading && userData) ? (<>Chargement des utilisateurs en cours... </>) : (
                                userData.map((value, index) => {
                                    return <span key={value.id} >
                                        <Link to={`/account/` + value.id}  onContextMenu={(e)=>  {e.preventDefault(); return false;}}>
                                            <button data-tip data-for={value.name}  value={value.id} className='btn btn-user p-md-3 p-1 btn-outline-light m-1' >
                                                <b>{value.name}</b>
                                            </button>
                                        </Link>
                                        <ReactTooltip id={value.name} place='bottom' effect='solid' clickable={true} delayShow={100} delayHide={200} >
                                            <button className=' btn' onClick={() => {
                                                if (window.confirm("Voulez-vous vraiment supprimer cet utilisateur ?") === true) {
                                                    deleteEntry(value.id)
                                                }
                                            }}><i className='text-danger fas fa-trash'></i></button>
                                        </ReactTooltip>
                                    </span>
                                }))}
                            {userData.length < 5 && <button className='btn btn-user p-1 p-md-3 btn-outline-warning m-1' onClick={() => { setOpen(true) }}><b>+</b></button>}
                        </div>
                </div>
            </div> : <AddUser onOpen={switchOpen} />
    )
}