import moment from "moment";
import "moment/locale/fr";
import { useEffect, useState } from "react"
import { useHistory, useParams } from "react-router-dom"
import { AddOp } from "../components/AddOp";
import { OpList } from "../components/OpList";
import { UpdateUser } from "../components/UpdateAcc";
import { budgetById } from "../services/budget-service";
import { findAllCat } from "../services/category-services";
import { operationbyUserId } from "../services/operation-services";




export const Account = () => {
    const [user, setUser] = useState({})
    const [edit, setEdit] = useState(false);
    const [loading, setLoading] = useState(true);
    const [totalOp, setTotalOp] = useState(0);
    const [moneyLeft, setMoneyLeft] = useState(0);
    const [userOp, setUserOp] = useState([]);
    const [category, setCategory] = useState([]);
    const [total, setTotal] = useState(0);
    const [newOp, setNewOp] = useState(false);

    let { id } = useParams();
    let history = useHistory();

    const redirect = () => {
        history.push('/')
    }

    useEffect(() => {

        async function fetchData() {
            let budget = await budgetById(id);

            if (budget != null) {
                let operation = await operationbyUserId(id, moment(budget.date_start).format('YYYY-MM-DD'), moment(budget.date_end).format('YYYY-MM-DD'));
                setUser(budget);
                let cat = await findAllCat()
                let tot = 0;
                setCategory(cat);
                if (operation != null && cat != null) {
                    operation.sort((a, b) => new Date(b.date) - new Date(a.date))
                    operation.forEach(element => {
                        tot += element.amount;
                    });
                    setUserOp(operation);

                }
                setTotal(Number(tot).toFixed(2));
                totalOp !== 0 && setMoneyLeft(Number(budget.total) + Number(totalOp))
            }

            
            setLoading(false);
        }
        fetchData();
        console.log('test');

        if (total !== 0) {
            setTotalOp(total)
        }

    }, [id, totalOp, total, newOp, edit])


    return (
        <div className="row account vh-100 vw-100">
            <div className="text-start position-absolute mt-2">
                <button className='btn btn-outline-success' onClick={redirect}>Retour</button>
            </div>
            <div className='row mx-auto flex-direction-row'>
                {!edit?<div className=' offset-md-1 offset-lg-2 col-12 mt-5 bg-success my-auto col-md-4 card text-light'>
                    <button onClick={() => { setEdit(!edit) }} className="btn position-absolute btn-success top-0 end-0 text-light far fa-edit"></button>
                    <div className="card-body">

                        <h2 className=' card-title col-12 text-nowrap'>{user.name}</h2>
                        <p className='card-text'>Sur la période du <br></br>
                            <b>{moment(user.date_start).format('ll')} </b> au <b>{moment(user.date_end).format('ll')} </b> <br /> il vous reste : </p>
                        <p className="rounded h4 bg-success  text-light">{Number(moneyLeft).toFixed(2) + '€ sur ' + Number(user.total).toFixed(2) + '€'}</p> <br></br>
                    </div>
                </div>:<UpdateUser value={user} id={id} close={()=>setEdit(false)}/>}
                <div className='offset-md-2 col-12 mt-5 col-md-4 offset-lg-1 col-lg-3'>
                    {newOp && <AddOp category={category} id={id} done={()=>setNewOp(false)}/>}
                    <div className={` text-light ${newOp ? 'h-50' : 'h-75'} overflow-auto rounded`}>
                        <div className="list-group">
                            {!loading && userOp.map((value, index) => { return <OpList key={value.op_id} value={value} category={category} refresh={()=>setUserOp(userOp.filter(item=>item.op_id !== value.op_id))} /> })}
                        </div>
                    </div>
                    <div>
                        <button onClick={()=>setNewOp(!newOp)} className='btn btn-warning mt-2'>Ajouter</button>
                    </div>
                </div>

            </div>

        </div>
    )
}