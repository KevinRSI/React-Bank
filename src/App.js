
import './css/App.css';
import {
    Switch,
    Route
} from "react-router-dom";
import { Home } from './pages/Home';
import { Account } from './pages/Account';
import { BGPart } from './components/bgParticles';



function App() {
    return (
        <div className="App container-fluid d-flex ">
            <Switch>
                <Route exact path='/'>
                    <BGPart />
                    <Home></Home>
                </Route>
                <Route path="/account/:id">
                    <Account />
                    <BGPart />
                </Route>
            </Switch>
        </div>
    );
}

export default App;
