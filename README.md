# Projet (en Français pour une fois)

Le but était de créer une application fullstack pour gérer son budget avec Node.JS et React.
J'ai décidé de complexifier un peu le brief en ajoutant une gestion basique d'utilisateurs ayant chacun un budget et des opérations différentes.

## Realisation

Premièrement j'ai créé un diagramme de classes avec mes entités :

![Diagramme de classe](readme_img/Diagramme%20budget.png)

Ensuite j'ai créé trois tables conforfément aux trois entités de mon diagramme dans la BDD grâce à un script sql et j'y ai inseré quelques données.

La partie suivante était donc le backend, j'ai utilisé Node.js avec express pour créer les routes et les repository de chaque entité et j'ai ajouté Joi.dev au tout pour créer des validations par schémas notamment pour les fonctions d'ajouts à la bdd.

![budget validator](readme_img/validator.png)

Pour le front j'ai utilisé React avec plusieurs librairies dont Particles et React-tooltip et Axios pour les requêtes AJAX.

![AJAX](readme_img/axios.png)

## Lien vers le backend et site final

[Backend](https://gitlab.com/KevinRSI/Node-Bank/)

[Site](https://kevinrsi.gitlab.io/React-Bank/)
